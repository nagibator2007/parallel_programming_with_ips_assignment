#include <cmath>
#include <iostream>

#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h>

#include <windows.h>
#include <mmsystem.h>

// integrated function
double f(double x) {
    return 4 / sqrt(4 - pow(x, 2));
}

// simple integration with n rectangles
double integral(double a, double b, unsigned int n) {
    const double step = (b - a) / n;
    cilk::reducer_opadd<double> sum(0.0);
    cilk_for(unsigned int i = 0; i < n; ++i) {
        *sum += f(a + (i + 0.5) * step);
    }
    return sum.get_value() * step;
}

int main() {
    // analytic solution is pi*2/3
    const double PI_2_3 = atan(1.0) * 8.0 / 3.0;
    const unsigned int NRUNS = 4;
    double value_old = PI_2_3;
    for(int unsigned nrun = 0; nrun < NRUNS; ++nrun) {
        const DWORD start_time = timeGetTime();
        double value_new = integral(0.0, 1.0, 1000);
        const DWORD end_time = timeGetTime();

        std::cout << value_new - value_old << std::endl;
        std::cout << end_time - start_time << "msec elapsed" << std:: endl;

        value_old = value_new;
    }
    return 0;
}